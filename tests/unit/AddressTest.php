<?php

use Codeception\Test\Unit;
use Lubisch\Coveralls\Address;

/**
 * Class ConfigTest
 */
class AddressTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * test that firstname works as expected
     */
    public function testFirstname()
    {
        $address = new Address();

        $address->setFirstname('Nick');

        self::assertEquals('Nick', $address->getFirstname());
    }

    public function testLastname()
    {
        $address = new Address();

        $address->setLastname('Lubisch');

        self::assertEquals('Lubisch', $address->getLastname());
    }

    public function testStreet()
    {
        $address = new Address();

        $address->setStreet('My Street 1');

        self::assertEquals('My Street 1', $address->getStreet());
    }

    public function testCity()
    {
        $address = new Address();

        $address->setCity('Bocholt');

        self::assertEquals('Bocholt', $address->getCity());
    }

    public function testZipcode()
    {
        $address = new Address();

        $address->setZipcode('46397');

        self::assertEquals('46397', $address->getZipcode());
    }

    public function testCountry()
    {
        $address = new Address();

        $address->setCountry('Germany');

        self::assertEquals('Germany', $address->getCountry());
    }
}
