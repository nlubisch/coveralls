# Testing Coveralls with Bitbucket Pipelines
Coveralls: [![Coverage Status](https://coveralls.io/repos/bitbucket/nlubisch/coveralls/badge.svg?branch=master)](https://coveralls.io/bitbucket/nlubisch/coveralls?branch=master)
Codecov: [![codecov](https://codecov.io/bb/nlubisch/coveralls/branch/master/graph/badge.svg)](https://codecov.io/bb/nlubisch/coveralls)

## How to use coveralls with Bitbucket Pipelines

### Composer
Install following package with composer:
```bash
$ composer require satooshi/php-coveralls
```

### .coveralls.yml

Add a new file ".coveralls.yml" in your project root with following content:
```yaml
repo_token: *your_token* # should be kept secret!

coverage_clover: tests/_output/coverage.xml #path to clover xml
json_path: tests/_output/coveralls.json #path to temporary json file for coveralls
```

The repo_token is optional because you can define an environment variable called "COVERALLS_REPO_TOKEN".


### Configure PHPUnit

The configuration for PHPUnit is well explained in the repository of the package [satooshi/php-coveralls](https://github.com/satooshi/php-coveralls#configuration).

### Configure Codeception

In order to use code coverage with Codeception you have to configure this in the global `codeception.yml` or the suite specific `.suite.yml` file.

```yaml
...
coverage:
    enabled: true # enable codecoverage
    include:
        - src/*.php #whitelist pattern for code coverage
...
```

Than you only have to run `codecept run unit --coverage-xml` which creates a file called `coverage.xml` in the `tests/_output/` directory.

### Send coverage data to Coveralls.io

Add following line to the script section in your `bitbucket-pipelines.yml`:
```yaml
...
pipelines:
  default:
    - step:
        script:
            ...
            - vendor/bin/coveralls
```

Thats it!



